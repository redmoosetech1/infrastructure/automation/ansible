FROM gitlab-registry.rmt/infrastructure/docker/runner:main

# Copy Infrastructure Playbooks
COPY collections /etc/ansible/collections
COPY inventory /etc/ansible/inventory
COPY lib /etc/ansible/lib
COPY playbooks /etc/ansible/playbooks
COPY roles /etc/anible/roles
COPY ./ssh /etc/ansible/ssh
COPY ansible.image.cfg /etc/ansible/ansible.cfg
RUN chmod -R 0600 /etc/ansible/
