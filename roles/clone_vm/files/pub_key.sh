#!/bin/bash
#
# Copyright (C) 2014-2020 - Point3 Security, Inc.
#
# This source code is the proprietary intellectual property of
# Point3 Security, Inc.  It is only provided to assist in
# the review process of finished products. Use of this code for
# other purposes requires a license.
#
runuser -l "$1" -c 'mkdir -p ~/.ssh'
runuser -l "$1" -c 'chmod 700 ~/.ssh'
runuser -l "$1" -c "eval $(ssh-agent -s) && echo $2 | tr -d '\r' | ssh-add -"
