Role Name
=========
This role will simply deploy VCSA Appliance to an ESX server using the vcsa binary.

Requirements
------------

Role Variables
--------------

iso_path: ""

# New VCSA
esxi_hostname: ""
esxi_username: ""
esxi_password: ""
vcsa_deployment_network: ""
vcsa_datastore: ""

# Appliance
deployment_option: ""
vm_name: ""

# Network
ip_address: ""
dns1: ""
dns2: ""
prefix: ""
gateway: ""
system_name: ""

# OS
vcsa_password: ""

# SSO
sso_password: ""
vcenter_domain_name: ""

# NTP
ntp: ""
Dependencies
------------

Example Playbook
----------------


License
-------


Author Information
------------------
