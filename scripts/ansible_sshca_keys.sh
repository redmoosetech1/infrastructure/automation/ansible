#!/bin/bash

step ssh certificate --provisioner client \
    --insecure --no-password \
    --principal administrator \
    "ansible@redmoose.tech" \
    "ansible_ecdsa_key"
