#!/bin/bash

# Install apt packages
sudo apt -y update
sudo apt -y install sshpass \
git \
software-properties-common \
python3.9 \
python3.9-dev \
python3.9-dbg \
python3.9-venv \
python3-pip \
libssh-dev

python3.9 -m pip install pip --upgrade

export PATH="$PATH:$HOME/.local/bin:$HOME/.bin"

# Install Pip modules
python3 -m pip install pyvmomi \
pyOpenSSL \
ansible \
ansible-pylibssh \
python-gitlab \
netaddr \
hvac \
--user

# Install Ansible Galaxy Packages
ansible-galaxy collection install community.vmware \
community.crypto \
community.docker \
community.hashi_vault \
ansible.posix \
community.general

# Install dependencies for Ansible Galaxy
python3 -m pip install -r ~/.ansible/collections/ansible_collections/community/vmware/requirements.txt --user
