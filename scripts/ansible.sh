#!/bin/sh
export ANSIBLE_CONFIG="/etc/ansible/ansible.cfg"
sudo cp -r ansible.cfg inventory/ roles/ /etc/ansible
exec ansible "${@}"
